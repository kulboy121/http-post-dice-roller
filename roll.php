<?php
$rollraw = strtolower($_POST["text"]);
if (substr($rollraw, 0, 2) == "r ") {
    $rollNoLoop = str_replace('r ', '', $rollraw);
} elseif (substr($rollraw, 0, 1) == "r" && !(substr($rollraw, 1, 1) == " ") && !(substr($rollraw, 1, 1) == "o")) {
    $rollNoLoop = str_replace('r', '', $rollraw);
} elseif (substr($rollraw, 0, 2) == "/r") {
    $rollNoLoop = str_replace('/roll ', '', $rollraw);
} else {
    $rollNoLoop = str_replace('roll ', '', $rollraw);
}
$rollExploded = explode('+', $rollNoLoop);
$rollMod = (int)$rollExploded[1];
$rollnet = $rollExploded[0];

$channel = "#" . $_POST["channel_name"];

$dtake = 0;
$atake = 0;
$disadv1 = 0;
$disadv2 = 0;
$adv1 = 0;
$adv2 = 0;
$percent10 = -1;
$percentP = "";
$polarRoll = array();
$percent = -1;
$finalroll = -1;

if ($rollnet == 'd') {
  $disadv1 = rand('1', 20);
  $disadv2 = rand('1', 20);
  if ($disadv1 < $disadv2) {
    $dtake = 1;
  } else {
    $dtake = 2;
  }
} elseif ($rollnet == 'a') {
  $adv1 = rand('1', 20);
  $adv2 = rand('1', 20);
  if ($adv1 > $adv2) {
    $atake = 1;
  } else {
    $atake = 2;
  }

} elseif ($rollnet == 'c') {

  $rolls = 1;
  $die = 20;

  $i = 1;

  while ($i <= $rolls) {
    $randnum = rand('1', $die);
    $finalroll = $finalroll + $randnum;
    if($i == $rolls) {
      $finalroll = $finalroll + 1;
    }
    $i++;
  }
} elseif ($rollnet == '%') {

  $percent10 = rand('1', 10);
  $percentDie = rand('0', 9);
  $percent = ($percentDie * 10) + $percent10;
  switch ($percentDie) {
    case 0:
      $percentP = "00";
      break;
    case 1:
      $percentP = "10";
      break;
    case 2:
      $percentP = "20";
      break;
    case 3:
      $percentP = "30";
      break;
    case 4:
      $percentP = "40";
      break;
    case 5:
      $percentP = "50";
      break;
    case 6:
      $percentP = "60";
      break;
    case 7:
      $percentP = "70";
      break;
    case 8:
      $percentP = "80";
      break;
    default:
      $percentP = "90";
      break;
  }

} elseif($rollnet == "polarity" || $rollnet == "polar" || $rollnet == "pol" || $rollnet == "p" || $rollnet == "f") {

  do {
    $tempRoll = rand(0,2);

    switch ($tempRoll) {
      case 0:
        $polarRoll[] = "-";
        break;
      case 1:
        $polarRoll[] = "0";
        break;
      case 2:
        $polarRoll[] = "+";
        break;
      }
  } while(count($polarRoll) < 4);
} else {

  $roll = explode('d', $rollnet);

  $numberofdice = $roll[0];
  $rolls = (int)$numberofdice;
  $die = (int)$roll[1];

  $i = 1;

  if ($rolls > 1000000) {
    $finalroll = -2;
  } elseif ($die > 1000000) {
    $finalroll = -2;
  } else {
    while ($i <= $rolls) {
      $randnum = rand('1', $die);
      $finalroll = $finalroll + $randnum;
      if($i == $rolls) {
        $finalroll = $finalroll + 1;
      }
      $i++;
    }
  }
}

$actualFinalRoll = 0;
$returntext = 'UNSPECIFIED ERROR; PING @jonak: FIX ME PLEASE!';
if ($rollMod == 0) {
  if ($_POST["user_name"] == '') {
    $returntext = 'Someone is accessing this from the web -- that does not work!';
  } elseif ($dtake == 1) {
    $returntext = '@' . $_POST["user_name"] . ' rolled with disadvantage: the rolls were ' . $disadv1 . ' and ' . $disadv2 . '. Thus, the outcome was ' . $disadv1 . '.';
    $actualFinalRoll = $disadv1;
  } elseif ($dtake == 2) {
    $returntext = '@' . $_POST["user_name"] . ' rolled with disadvantage: the rolls were ' . $disadv1 . ' and ' . $disadv2 . '. Thus, the outcome was ' . $disadv2 . '.';
    $actualFinalRoll = $disadv2;
  } elseif ($atake == 1) {
    $returntext = '@' . $_POST["user_name"] . ' rolled with advantage: the rolls were ' . $adv1 . ' and ' . $adv2 . '. Thus, the outcome was ' . $adv1 . '.';
    $actualFinalRoll = $adv1;
  } elseif ($atake == 2) {
    $returntext = '@' . $_POST["user_name"] . ' rolled with advantage: the rolls were ' . $adv1 . ' and ' . $adv2 . '. Thus, the outcome was ' . $adv2 . '.';
    $actualFinalRoll = $adv1;
  } elseif ($percent10 != -1) {
    $actualFinalRoll = $percent;
    $returntext = '@' . $_POST["user_name"] . ' rolled percentile: the rolls were ' . $percentP . ' and ' . $percent10 . '. Thus, the outcome was ' . $percent . '%.';
  } elseif ($polarRoll != array()) {
      $actualFinalRoll = $polarRoll;
      $returntext = '@' . $_POST["user_name"] . ' rolled a fate (polar) die. The primary polarity was *' . $polarRoll[0] . '*, with auxiliary polarities of *' . $polarRoll[1] . ', ' . $polarRoll[2] . ', and ' . $polarRoll[3] . '*.';
  } elseif ($finalroll == -1) {
    $returntext = 'An error occured with the roll of @' . $_POST["user_name"] . '.';
  } elseif ($finalroll == -2) {
    $returntext = 'The roll of @' . $_POST["user_name"] . ' was too large';
  } elseif ($rollnet == "c") {
    $returntext = '@' . $_POST["user_name"] . ' rolled a standard check (1d20) and the result was ' . $finalroll . '.';
    $actualFinalRoll = $finalroll;
  } else {
    $returntext = '@' . $_POST["user_name"] . ' rolled ' . $rollnet . ' and the result was ' . $finalroll . '.';
    $actualFinalRoll = $finalroll;
  }
} else {
  if ($_POST["user_name"] == '') {
    $returntext = 'Someone is accessing this from the web -- that does not work!';
  } elseif ($dtake == 1) {
    $actualFinalRoll = $disadv1 + $rollMod;
    $returntext = '@' . $_POST["user_name"] . ' rolled with disadvantage: the rolls were ' . $disadv1 . ' and ' . $disadv2 . '. Thus, the outcome was ' . $disadv1 . ' with a modifier of ' . $rollMod . ' for a final value of ' . $actualFinalRoll . '.';
  } elseif ($dtake == 2) {
    $actualFinalRoll = $disadv2 + $rollMod;
    $returntext = '@' . $_POST["user_name"] . ' rolled with disadvantage: the rolls were ' . $disadv1 . ' and ' . $disadv2 . '. Thus, the outcome was ' . $disadv2 . ' with a modifier of ' . $rollMod . ' for a final value of ' . $actualFinalRoll . '.';
  } elseif ($atake == 1) {
    $actualFinalRoll = $adv1 + $rollMod;
    $returntext = '@' . $_POST["user_name"] . ' rolled with advantage: the rolls were ' . $adv1 . ' and ' . $adv2 . '. Thus, the outcome was ' . $adv1 . ' with a modifier of ' . $rollMod . ' for a final value of ' . $actualFinalRoll . '.';
  } elseif ($atake == 2) {
    $actualFinalRoll = $adv2 + $rollMod;
    $returntext = '@' . $_POST["user_name"] . ' rolled with advantage: the rolls were ' . $adv1 . ' and ' . $adv2 . '. Thus, the outcome was ' . $adv2 . ' with a modifier of ' . $rollMod . ' for a final value of ' . $actualFinalRoll . '.';
  } elseif ($percent10 != -1) {
    $actualFinalRoll = $percent + $rollMod;
    $returntext = '@' . $_POST["user_name"] . ' rolled percentile: the rolls were ' . $percentP . ' and ' . $percent10 . '. Thus, the outcome was ' . $percent . '% with a modifier of ' . $rollMod . '% for a final value of ' . $actualFinalRoll . '%.';
  } elseif ($polarRoll != array()) {
      $actualFinalRoll = $polarRoll;
      $returntext = '@' . $_POST["user_name"] . ' rolled a fate (polar) die. The primary polarity was *' . $polarRoll[0] . '*, with auxiliary polarities of *' . $polarRoll[1] . ', ' . $polarRoll[2] . ', and ' . $polarRoll[3] . '*.';
  } elseif ($finalroll == -1) {
    $returntext = 'An error occured with the roll of @' . $_POST["user_name"] . '.';
  } elseif ($finalroll == -2) {
    $returntext = 'The roll of @' . $_POST["user_name"] . ' was too large';
  } elseif ($rollnet == "c") {
    $actualFinalRoll = $finalroll + $rollMod;
    $returntext = '@' . $_POST["user_name"] . ' rolled a standard check (1d20) and the result was ' . $finalroll .' with a modifier of ' . $rollMod . ' for a final value of ' . $actualFinalRoll . '.';
  } else {
    $actualFinalRoll = $finalroll + $rollMod;
    $returntext = '@' . $_POST["user_name"] . ' rolled ' . $rollnet . ' and the result was ' . $finalroll . ', with a modifier of ' . $rollMod . ', boosting the result to ' . $actualFinalRoll . '.';
  }
}

$data = array(
  'text' => $returntext,
  'channel' => $channel,
);

$url = "https://hooks.slack.com/services/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";
$content = json_encode($data);


$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER,
        array("Content-type: application/json"));
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

$json_response = curl_exec($curl);
$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

$rollLog = "\n" . $actualFinalRoll;

if(!($finalroll == -1 || $finalroll == -2)) {
  file_put_contents("rollLog.csv", $rollLog, FILE_APPEND);
}
?>
